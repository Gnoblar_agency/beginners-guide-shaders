﻿Shader "Custom/BlendTextures" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SecondTex("Second Albedo (RGB)", 2D) = "white" {}
		_Blend("Blend amount", range(0, 1)) = 0
		[Space]
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _SecondTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Blend;
		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Sample each texture using the same UV coordinates
			fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = tex2D(_SecondTex, IN.uv_MainTex) * _Color;
			
			// Linear interpolate between the two using out "blend" parameter
			fixed4 blended = lerp(c1, c2, _Blend);

			o.Albedo = blended.rgb;

			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = blended.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
