﻿Shader "Custom/Hologram/Star Wars - Basic" 
{
	Properties 
	{
		[Header(Texture and Color)]
		[HDR]
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Lines Texture", 2D) = "white" {}

		[Space]
		[Header(Animation)]
		_LineSpeed("Line Animation Speed", range(-5, 5)) = 0
		_FlickerAmount("Flicker Amount", range(0, 0.95)) = 0

		[Space]
		[Header(Rim light)]
		[HDR]
		_RimColor ("Rim Light Color", Color) = (1, 1, 1, 1)
		_RimIntensity ("Rim Light Intensity", range(0.05, 10)) = 2
		
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows alpha:blend

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		
		//Link our properties in the shader
		sampler2D _MainTex;
		float4 _MainTex_ST;

		fixed4 _Color;
		fixed4 _RimColor;

		float _RimIntensity;
		float _LineSpeed;
		float _FlickerAmount;

		struct Input 
		{
			float3 uv_MainTex;
			float3 viewDir;
			float4 screenPos;
		};

		//Helper function to generate a random float number. Random generation is not built into CG
		float random(float2 p)
		{
			float2 K1 = float2(
				23.14069263277926, // e^pi (Gelfond's constant)
				2.665144142690225 // 2^sqrt(2) (Gelfondâ€“Schneider constant)
				);
			return frac(cos(dot(p, K1)) * 12345.6789);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			// Albedo comes from a lines texture tinted by color

			//However, we want the UV to be in screenspace, convert it over by dividing by w
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;
			//Apply the scale and offset from in the editor
			screenUV = screenUV * _MainTex_ST.xy + _MainTex_ST.zw;
			//Animate the lines moving up
			screenUV.y += -_LineSpeed * _Time.y;

			fixed4 lines = tex2D(_MainTex, screenUV);

			//Set emission to tinted sample
			o.Emission = lines.rgb * _Color;

			//Calculate the rim lighting
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			fixed4 rimColor = _RimColor * pow(rim, _RimIntensity);
			o.Emission += rimColor.rgb;

			//Set the transparency, favoring the highest alpha
			fixed alpha = max(lines.a, rimColor.a);

			float flicker =  random(float2(_Time.y, _Time.w));

			if (flicker < _FlickerAmount)
			{
				alpha = 0;
			}

			o.Alpha = alpha;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
