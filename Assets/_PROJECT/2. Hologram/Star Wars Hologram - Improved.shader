﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Daniel/Hologram/Star Wars - Improved" 
{
	Properties 
	{
		[Header(Texture and Color)]
		[HDR]
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Lines Texture", 2D) = "white" {}
		_NormalTex("Normal Map", 2D) = "bump" {}

		[Space]
		[Header(Animation)]
		_LineSpeed("Line Animation Speed", range(-5, 5)) = 0
		_FlickerAmount("Flicker Amount", range(0, 0.95)) = 0
		_DisplaceAmount("Displace Amount", range(0, 0.95)) = 0
		_DisplaceDistance("Displace Distance", range(0.01, 1)) = 0

		[Space]
		[Header(Rim light)]
		[HDR]
		_RimColor("Rim Light Color", Color) = (1, 1, 1, 1)
		_RimIntensity("Rim Light Intensity", range(0.05, 10)) = 0

		_Right("Right", vector) = (1, 0, 0, 0)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200

		Stencil
		{
			Ref 1
			Comp Greater
			Pass Replace
		}

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows vertex:vert alpha:blend exclude_path:deferred

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		
		//Link our properties in the shader
		sampler2D _MainTex;
		sampler2D _NormalTex;

		float4 _MainTex_ST;
		float4 _Right;

		fixed4 _Color;
		fixed4 _RimColor;

		float _RimIntensity;
		float _LineSpeed;
		float _FlickerAmount;
		float _DisplaceAmount;
		float _DisplaceDistance;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalTex;
			float3 viewDir;
			float4 screenPos;
		};

		//Helper function to generate a random float number. Random generation is not built into CG
		float random(float2 p)
		{
			float2 K1 = float2(
				23.14069263277926, // e^pi (Gelfond's constant)
				2.665144142690225 // 2^sqrt(2) (Gelfondâ€“Schneider constant)
				);
			return frac(cos(dot(p, K1)) * 12345.6789);
		}

		void vert(inout appdata_full v) 
		{
			//Get a random number to determine if we displace
			float displace = random(float2(_Time.x, 34.1354));
			displace = step(displace, _DisplaceAmount);

			float3 worldspaceDisplacement = _Right * _DisplaceDistance * displace * sign(0.5 - random(float2(_CosTime.x, -12.522)));

			v.vertex.xyz += mul(unity_WorldToObject, worldspaceDisplacement);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			o.Normal = UnpackNormal(tex2D(_NormalTex, IN.uv_NormalTex));

			//We want the UVs to be in screenspace, convert it over by dividing by w
			float2 screenUV = IN.screenPos.xy / IN.screenPos.w;

			//Create a second set of UVs that will animate in the opposite direction
			float2 screenUV2 = screenUV * fixed2(2, 2) + fixed2(0, 0.5);

			//Apply the scale and offset from in the editor
			screenUV = screenUV * _MainTex_ST.xy + _MainTex_ST.zw;
			

			//Animate the lines moving up and down
			screenUV.y += -_LineSpeed * _Time.y;
			screenUV2.y -= -_LineSpeed * 0.5 * _Time.y;

			//Sample the first set of lines
			fixed4 lines = tex2D(_MainTex, screenUV);

			//Sample the second set of lines and add them to the first, darken them a bit as well
			lines += tex2D(_MainTex, screenUV2) * 0.65;

			//Set emission to tinted sample
			o.Emission = lines.rgb * _Color;

			//Calculate the rim lighting
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			fixed4 rimColor = _RimColor * pow(rim, _RimIntensity);
			o.Emission += rimColor.rgb;

			//Set the transparency, favoring the highest alpha
			fixed alpha = max(lines.a, rimColor.a);

			float flicker =  random(float2(_Time.y, _Time.w));

			if (flicker < _FlickerAmount)
			{
				alpha = 0;
			}

			o.Alpha = alpha;
		}
		ENDCG
	}
}
