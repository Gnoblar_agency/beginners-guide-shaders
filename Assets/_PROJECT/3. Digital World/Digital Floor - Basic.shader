﻿Shader "Custom/Hologram/Digital Floor - Basic" {
	Properties {
		[Color and Textures]
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		[Space]
		[Header(Effect Settings)]
		_Radius("Reveal Radius", range(0.1, 10)) = 2

		[HideInInspector]
		_Target ("Worldspace position of the target", vector) = (0,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf VirtualSurface fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#include "UnityPBSLighting.cginc"

		sampler2D _MainTex;

		float4 _Target;
		fixed4 _Color;

		float _Radius;


		//Define out custom lighting the makes everything unlit
		inline half4 LightingVirtualSurface(SurfaceOutputStandard s, half3 lightDir, UnityGI gi)
		{
			//Select lighting based on if we are rendering the virtual or real object

			return fixed4(0, 0, 0, 0); // Unlit
		}


		//Add in the mandatory GI function
		inline void LightingVirtualSurface_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi)
		{
			LightingStandard_GI(s, data, gi);
		}

		struct Input 
		{
			float2 uv_MainTex;
			float3 worldPos;
		};


		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			
			
			float dist = distance(IN.worldPos, _Target.xyz);

			if (dist > _Radius)
			{
				o.Emission = c.rgb;
			}
			else
			{
				o.Emission = _Color;
			}

			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
