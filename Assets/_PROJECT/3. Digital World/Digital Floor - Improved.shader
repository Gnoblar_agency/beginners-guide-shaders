﻿Shader "Custom/Hologram/Digital Floor - Improved" {
	Properties {
		[Color and Textures]
		[HDR]
		_Color ("Color", Color) = (1,1,1,1)
		[HDR]
		_FalloffColor("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}

		[Space]
		[Header(Effect Settings)]
		_Radius("Reveal Radius", range(0.1, 10)) = 2
		_FalloffSize("Falloff percent", range(0.01, 1)) = 0.5
		_PatternTex("Pattern Texture", 2D) = "white" {}
		_PatternThreshold("Pattern sharpness", range(0, 1)) = 1

		[HideInInspector]
		_Target ("Worldspace position of the target", vector) = (0,0,0,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf VirtualSurface fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0
		#include "UnityPBSLighting.cginc"

		sampler2D _MainTex;
		sampler2D _PatternTex;

		float4 _Target;
		fixed4 _Color;
		fixed4 _FalloffColor;

		float _Radius;
		float _FalloffSize;
		float _PatternThreshold;
		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_PatternTex;
			float3 worldPos;
		};

		//Define out custom lighting the makes everything unlit
		inline half4 LightingVirtualSurface(SurfaceOutputStandard s, half3 lightDir, UnityGI gi)
		{
			//Select lighting based on if we are rendering the virtual or real object

			return fixed4(0,0,0,0); // Unlit
		}


		//Add in the mandatory GI function
		inline void LightingVirtualSurface_GI(SurfaceOutputStandard s, UnityGIInput data, inout UnityGI gi)
		{
			LightingStandard_GI(s, data, gi);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			//Sample the texture and apply the tint, this will be used to show unrevealed areas
			fixed4 mainPattern = tex2D (_MainTex, IN.uv_MainTex);
			
			//Get the distance of the current fragment to the target
			float dist =  distance(IN.worldPos, _Target.xyz);

			//Determine how big the falloff area is. Also animate the falloff slightly
			float falloff = _Radius * (_FalloffSize - (sin(_Time.y * 3 + 12) * 0.015));

			//Use Emission because I want it to glow when using the bloom effect. 
			//If the distance is more than the radius, show the grid
			if (dist > _Radius)
			{
				//Show the grid
				//o.Emission = hiddenPattern.rgb * _Color;
				o.Emission = fixed4(0, 0, 0, 0);

			}
			else if (dist > falloff)
			{
				//Get a value of 0-1 where 0 is directly on the edge of the solid section
				float normalizedDist = 1 - ((dist - falloff)  / (_Radius - falloff));

				//Get the color at this point as a mix between our normal and falloff colors
				fixed3 color = lerp(_FalloffColor, _Color, (normalizedDist));

				//Calculate two pulses along the radius
				float pulse1 = pow(1 - frac((_Time.y + 1) * 0.25 - (normalizedDist)), 9);
				float pulse2 = pow(1 - frac(_Time.y * 0.25 - (1 - normalizedDist)), 9);

				//Show the floor with a fade based on the bricks
				float pattern = tex2D(_PatternTex, IN.uv_PatternTex).r * normalizedDist * 2;

				//Animate the pattern with two sin waves
				pattern -= (sin(_Time.y * 7 + 3) * 0.015) + (sin(_Time.y * 10 + _Time.z) * 0.023);

				//Empower the pattern with the pulse
				pattern += pattern * (pulse1 + pulse2);

				//Create a blend between distance and pattern Sample
				pattern = lerp (pattern.r , 1, pow(normalizedDist,2));
				
				//Cutout pixels that are less bright than the threshold.
				if (pattern < _PatternThreshold)
					pattern = saturate(pattern - 0.5);

				o.Emission = pattern * color;
			}
			else
			{
				//Render the solid section
				o.Emission = _Color;
			}

			//o.Emission *= 0.9 + (sin(_Time.y * 10 + 3) * 0.2);
			o.Emission += mainPattern * _Color;

			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
