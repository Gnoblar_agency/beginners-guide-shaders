Shader "Hidden/Daniel/EdgeHighlight"
{
	HLSLINCLUDE
	
		#include "../PostProcessing/Shaders/StdLib.hlsl"

		TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
		TEXTURE2D_SAMPLER2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture);
		TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);

		uniform float4 _MainTex_TexelSize;

		float _Blend;
		float _Threshold;
		half4 _Highlight;

		inline float DecodeFloatRG( float2 enc )
		{
			float2 kDecodeDot = float2(1.0, 1/255.0);
			return dot( enc, kDecodeDot );
		}


		float4 Frag(VaryingsDefault i) : SV_Target
		{
			//Sample fragment color
			float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, i.texcoord);

			float4 center, right, left, up, down;

			//float2 samples[5] = {float2(0,0), float2(-1,0),float2(0,1),float2(0,1),float2(0,-1)};

			//Sample depth normals
			center = SAMPLE_TEXTURE2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture, i.texcoord);
			right = SAMPLE_TEXTURE2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture, i.texcoord + (_MainTex_TexelSize * float2(1,0)) );
			left = SAMPLE_TEXTURE2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture, i.texcoord + (_MainTex_TexelSize * float2(-1,0)) );
			up = SAMPLE_TEXTURE2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture, i.texcoord + (_MainTex_TexelSize * float2(0,1)) );
			down = SAMPLE_TEXTURE2D(_CameraDepthNormalsTexture, sampler_CameraDepthNormalsTexture, i.texcoord + (_MainTex_TexelSize * float2(0,-1)) );

			float3 centerN, rightN, leftN, upN, downN;
			float3 centerD, rightD, leftD, upD, downD;

			centerN = DecodeViewNormalStereo(center);
			leftN = DecodeViewNormalStereo(left);
			rightN = DecodeViewNormalStereo(right);
			upN = DecodeViewNormalStereo(up);
			downN = DecodeViewNormalStereo(down);

			centerD = DecodeFloatRG(center.zw);
			rightD = DecodeFloatRG(right.zw);
			leftD = DecodeFloatRG(left.zw);
			upD = DecodeFloatRG(up.zw);
			downD = DecodeFloatRG(down.zw);

			//Sample depth
			//depthSample = Linear01Depth(SAMPLE_TEXTURE2D(_CameraDepthTexture, sampler_CameraDepthTexture, i.texcoord).x).xxxx;
			float averageDepthDistance = (abs(centerD - rightD) + abs(centerD - leftD) + abs(centerD - upD) + abs(centerD - downD)) / 4.0;
			float3 averageNormalDistance = (abs(centerN - rightN) + abs(centerN - leftN) + abs(centerN - upN) + abs(centerN - downN)) / 4.0;

			if (averageDepthDistance > _Threshold || (averageNormalDistance.x + averageNormalDistance.y + averageNormalDistance.z) > _Threshold * 2)
			{
				return half4(0, 0, 0, 1);
			}
			return half4(1, 1, 1, 1);
			//return color;
		}

		
	ENDHLSL
	
	SubShader
	{
		Cull Off ZWrite Off ZTest Always
	
		Pass
		{
			HLSLPROGRAM
			
			#pragma vertex VertDefault
			#pragma fragment Frag
			
			ENDHLSL
		}
	}
}
