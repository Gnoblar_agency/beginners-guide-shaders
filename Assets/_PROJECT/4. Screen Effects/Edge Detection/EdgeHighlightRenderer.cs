﻿using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;


[Serializable]
[PostProcess(typeof(EdgeHighlightRenderer), PostProcessEvent.AfterStack, "Custom/EdgeHighlight")]
public sealed class EdgeHighlight : PostProcessEffectSettings
{
    [Range(0.01f, 2f)]
    public FloatParameter threshold = new FloatParameter { value = 0.1f };
}


public sealed class EdgeHighlightRenderer : PostProcessEffectRenderer<EdgeHighlight>
{

    public override DepthTextureMode GetCameraFlags()
    {
        return DepthTextureMode.DepthNormals | DepthTextureMode.Depth;
    }

    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/Daniel/EdgeHighlight"));

		sheet.properties.SetFloat("_Threshold", settings.threshold);

        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}
