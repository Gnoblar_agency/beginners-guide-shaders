﻿Shader "Hidden/Output UV"
{
	//Outputs UV coordinates to the screen
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			//Note, we return a float as we want to maintain precision on uv coords
			float4 frag (v2f i) : SV_Target
			{
				return float4(i.uv.x, i.uv.y, _MainTex_ST.x, _MainTex_ST.y);		
			}
			ENDCG
		}
	}
}
