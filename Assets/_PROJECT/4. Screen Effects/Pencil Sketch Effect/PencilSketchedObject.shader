﻿// http://kylehalladay.com/blog/tutorial/2017/02/21/Pencil-Sketch-Effect.html
Shader "Daniel/Sketch/PencilSketchedObject"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Hatch0 ("Dark Hatching Texture", 2D) = "white" {}
		_Hatch1 ("Light Hatching Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode" = "ForwardBase" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityStandardBRDF.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 normal : TEXCOORD1;
			};

			sampler2D _Hatch0;
			sampler2D _Hatch1;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			half3 SampleHatchTexture(half intensity, float2 uv)
			{	
				
				//We are sampling from 6 hatch textures
				half i = intensity * 6;
				half3 intensity3 = half3(i, i, i);

				half3 overbright = max(0, intensity - 1.0);

				half3 weights0 = saturate(intensity3 - half3(0, 1, 2));
				half3 weights1 = saturate(intensity3 - half3(3, 4, 5));

				//Ensure we only have 2 non-zero weights
				weights0.xy -= weights0.yz;
				weights0.z -= weights1.x;
				//Double check this line
				weights1.xy -= weights1.yz;

				half3 hatch0 = tex2D(_Hatch0, uv).rgb;
				half3 hatch1 = tex2D(_Hatch1, uv).rgb;

				hatch0 = hatch0 * weights0;
				hatch1 = hatch1 * weights1;

				half3 hatching = overbright + hatch0.r +
				    hatch0.g + hatch0.b +
				    hatch1.r + hatch1.g +
				    hatch1.b;

				return hatching;
			}

			half4 frag (v2f i) : SV_Target
			{
				// sample the texture
				half4 col = tex2D(_MainTex, i.uv);

				half3 diffuse = col.rgb * _LightColor0.rgb * DotClamped(_WorldSpaceLightPos0.xyz, i.normal.xyz);

				half intensity = dot(diffuse, half3(0.2326, 0.7152, 0.0722));

				col.rgb = SampleHatchTexture(intensity, i.uv * 8);

				return col;
			}
			ENDCG
		}
	}
}
