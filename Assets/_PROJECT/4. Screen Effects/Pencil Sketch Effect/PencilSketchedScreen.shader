﻿Shader "Daniel/Sketch/Pencil Sketched Screen Effect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Hatch0 ("Dark Hatching Texture", 2D) = "white" {}
		_Hatch1 ("Light Hatching Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvFlipY : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.uvFlipY = o.uv;

				#if defined(UNITY_UV_STARTS_AT_TOP) && !defined(SHADER_API_MOBILE)
				o.uvFlipY.y = 1.0 - o.uv.y;
				#endif

				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _Hatch0;
			sampler2D _Hatch1;
			sampler2D _UVBuffer;

			half3 SampleHatchTexture(half intensity, float2 uv)
			{	
				
				//We are sampling from 6 hatch textures
				half i = intensity * 6;
				half3 intensity3 = half3(i, i, i);

				half3 overbright = max(0, intensity - 1.0);

				half3 weights0 = saturate(intensity3 - half3(0, 1, 2));
				half3 weights1 = saturate(intensity3 - half3(3, 4, 5));

				//Ensure we only have 2 non-zero weights
				weights0.xy -= weights0.yz;
				weights0.z -= weights1.x;
				//Double check this line
				weights1.xy -= weights1.yz;

				half3 hatch0 = tex2D(_Hatch0, uv).rgb;
				half3 hatch1 = tex2D(_Hatch1, uv).rgb;

				hatch0 = hatch0 * weights0;
				hatch1 = hatch1 * weights1;

				half3 hatching = overbright + hatch0.r +
				    hatch0.g + hatch0.b +
				    hatch1.r + hatch1.g +
				    hatch1.b;

				return hatching;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				half4 col = tex2D(_MainTex, i.uv);

				float4 uv = tex2D(_UVBuffer, i.uvFlipY);

				half intensity = dot(col, half3(0.2326, 0.7152, 0.0722));

				col.rgb = SampleHatchTexture(intensity, i.uv * 8);

				return col;
			}
			ENDCG
		}
	}
}
