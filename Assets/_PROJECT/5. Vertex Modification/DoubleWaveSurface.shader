﻿Shader "Daniel/Waves/Double" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
	_Tessellation("Tessellation", Range(1,32)) = 1
		[Space]
		_Period ("Amount of waves per second", range(1.0,100.0)) = 1
		_Phase ("Start Phase of wave", range(0.0,1.0)) = 0
		_Amplitude ("Wave Height", range(0.1,10.0)) = 1

		_Scale ("Wave scale", range(0,10.0)) = 0

		_Smoothing ("Normal Smoothing", range(0.0,1.0)) = 0

		_XDrift ("X-Axis Secondary Wave Drift", range(0.1,10.0)) = 0
		_ZDrift ("Z-Axis Secondary Wave Drift", range(0.1,10.0)) = 0
	}
	SubShader {
		Tags  { "Queue"="Geometry" "RenderType"="Opaque" }

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf BlinnPhong vertex:vert tessellate:tess
		//#pragma surface surf BlinnPhong finalcolor:showNormals vertex:vert noforwardadd
		#pragma target 3.0

		sampler2D _MainTex;
		float _Period;
		float _Amplitude;
		float _XDrift;
		float _ZDrift;
		float _Phase;
		float _Smoothing;
		float _Scale;
		float _Tessellation;

		struct Input {
			float2 uv_MainTex;
			float4 vHeight;
		};

		float4 tess()
		{
			return _Tessellation;
		}

		void vert(inout appdata_full v)
		{
				
				//float3 vWorld = mul(_Object2World, v.vertex);
				float3 vWorld = v.vertex;

				//create false neigbours
				float3 vNeigbour1 = vWorld + float3(0.05,0,0);
				float3 vNeigbour2 = vWorld + float3(0,0,0.05);


				//Animation Correction values
				float phase = _Phase * (3.14 * 2);
				float phase2 = _Phase * (3.14 * 1.123);
				float speed = _Time.y * _Period;
				float speed2 = _Time.y * (_Period * 0.33);
				float Amplitude2 = _Amplitude * 1.0;

				float vWorldAlt = vWorld.x * _XDrift + vWorld.z * _ZDrift;
				float vNeigbour1Alt = vNeigbour1.x * _XDrift + vNeigbour1.z * _ZDrift;
				float vNeigbour2Alt = vNeigbour2.x * _XDrift + vNeigbour2.z * _ZDrift;

				//Displace vertices using sine wave
				vWorld.y += sin(phase + speed +(vWorld.x * _Scale) ) *_Amplitude;
				vWorld.y += sin(phase2 + speed2 +(vWorldAlt.x * _Scale) ) *Amplitude2;

				vNeigbour1.y += sin(phase + speed +(vNeigbour1.x * _Scale) ) *_Amplitude;
				vNeigbour1.y += sin(phase2 + speed2 +(vNeigbour1Alt.x * _Scale) ) *Amplitude2;

				vNeigbour2.y += sin(phase + speed +(vNeigbour2.x * _Scale) ) *_Amplitude;
				vNeigbour2.y += sin(phase2 + speed2 +(vNeigbour2Alt.x * _Scale) ) *Amplitude2;

				//smoothing the normals
				vNeigbour1.y -= (vNeigbour1.y - vWorld.y) * _Smoothing;
				vNeigbour2.y -= (vNeigbour2.y - vWorld.y) * _Smoothing;

				//calculate cross product to get new normal
				//float3 vNormalWorld = cross(vNeigbour2-vWorld, vNeigbour1-vWorld);
				float3 vNormal = cross(vNeigbour2-vWorld, vNeigbour1-vWorld);

	

				//Put normal back in object space
				//float3 vNormal = mul(_World2Object, vNormalWorld);

				//Normalize the normal
				v.normal = normalize(vNormal);

				//put vertex back into object space. Unity will do the MVP projection for us
				v.vertex.xyz = vWorld.xyz;
				
		}


		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			//fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			//o.Albedo = c.rgb;
			o.Albedo = c.rgb;
			o.Alpha = 1;
		}
		ENDCG



	}
	FallBack "VertexLit"
}
