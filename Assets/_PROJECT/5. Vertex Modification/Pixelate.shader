﻿Shader "Daniel/Pixelate" {
	Properties {
		
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecMap("Specular map", 2D) = "black"{}
		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		_BumpMap("Normal Map", 2D) = "bump" {}
		_Tessellation("Tessellation", Range(1,32)) = 1
		_Color ("Color", Color) = (1,1,1,1)
		_SnapValue ("Snap Value",range(0,5)) = 0
		_Pixels ("Screen Size in Pixels", Vector)= (10,10,10,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		Cull Back
		ZWrite On
		ZTest Less
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf StandardSpecular fullforwardshadows  tessellate:tess vertex:vert
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0 PIXELSNAP_ON

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _SpecMap;
		half _Glossiness;
		fixed4 _Color;

		fixed4 _Pixels;
		float _SnapValue;
		float _Tessellation;
		struct Input 
		{
			float2 uv_MainTex;
		};

		float4 tess()
		{
			return _Tessellation;
		}

		void vert(inout appdata_full v)
		{
			//Snap the vertices
			v.vertex.xyz = round((v.vertex.xyz * _Pixels.xyz) + _SnapValue) / _Pixels.xyz;

			#if defined(PIXELSNAP_ON)
				v.vertex = UnityPixelSnap(v.vertex);
			#endif
		}


		void surf (Input IN, inout SurfaceOutputStandardSpecular o) 
		{
			//Snap the UV coordinates to our gridsize
			float2 uv = round((IN.uv_MainTex * _Pixels.xy) + _SnapValue) / _Pixels.xy;

			fixed4 c = tex2D(_MainTex, uv) * _Color;

			o.Normal = UnpackNormal(tex2D(_BumpMap, uv)); 
			o.Specular = tex2D(_SpecMap, uv);
			o.Smoothness = _Glossiness;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG

		Pass {
    Name "ShadowCaster"
    Tags { "LightMode" = "ShadowCaster" }
           
    Fog {Mode Off}
    ZWrite On ZTest LEqual Cull Off
    Offset 1, 1
     
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag
    #pragma multi_compile_shadowcaster
    #pragma fragmentoption ARB_precision_hint_fastest
    #include "UnityCG.cginc"
 
    float4 _Pixels;
    float _SnapValue;
             
    struct v2f {
        V2F_SHADOW_CASTER;
    };
             
    v2f vert( appdata_base v ) {
        v.vertex.xyz = round(v.vertex.xyz*_Pixels.xyz+_SnapValue)/_Pixels.xyz;
        v2f o;
        TRANSFER_SHADOW_CASTER(o)
        return o;
    }
             
    float4 frag( v2f i ) : COLOR {
        SHADOW_CASTER_FRAGMENT(i)
    }
    ENDCG
}
       
// Pass to render object as a shadow collector
Pass {
    Name "ShadowCollector"
    Tags { "LightMode" = "ShadowCollector" }
           
    Fog {Mode Off}
    ZWrite On ZTest LEqual
     
    CGPROGRAM
    #pragma vertex vert
    #pragma fragment frag
    #pragma fragmentoption ARB_precision_hint_fastest
    #pragma multi_compile_shadowcollector
             
    #define SHADOW_COLLECTOR_PASS
    #include "UnityCG.cginc"
 
 	float4 _Pixels;
 	float _SnapValue;

    struct appdata 
    {
        float4 vertex : POSITION;
    };
             
    struct v2f 
    {
        V2F_SHADOW_COLLECTOR;
    };
             
    v2f vert (appdata v) 
    {
    	//Alter the vertices for blocky shadows
        v.vertex.xyz = round(v.vertex.xyz*_Pixels.xyz+_SnapValue)/_Pixels.xyz;
        v2f o;
        TRANSFER_SHADOW_COLLECTOR(o)
        return o;
    }
             
    fixed4 frag (v2f i) : COLOR {
        SHADOW_COLLECTOR_FRAGMENT(i)
    }
    ENDCG
}
 

	}
	FallBack "Diffuse"
}
