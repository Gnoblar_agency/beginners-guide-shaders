﻿Shader "Daniel/Waves/Single" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_Period("Amount of waves per second", range(1.0,100.0)) = 1
		_Amplitude("Wave Height", range(0, 10.0)) = 1
		_Scale("Wave scale", range(0.0, 10.0)) = 0
	}
	SubShader {
		Tags  { "Queue" = "Geometry" "RenderType" = "Opaque" }

		CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf BlinnPhong vertex:vert
			//#pragma surface surf BlinnPhong finalcolor:showNormals vertex:vert noforwardadd
			#pragma target 3.0

			sampler2D _MainTex;
			float _Period;
			float _Amplitude;
			float _Phase;
			float _Smoothing;
			float _Scale;

			struct Input {
				float2 uv_MainTex;
			};


			void vert(inout appdata_full v, out Input o)
			{
					UNITY_INITIALIZE_OUTPUT(Input, o);
					
					float3 vPos = v.vertex;

					//Animation values
					float phase = _Phase * (3.14 * 2);
					float speed = _Time.y * _Period;

					//Displace vertices using sine wave with our input values
					vPos.y += sin(speed + (vPos.x * _Scale)) *_Amplitude;

					//put vertex back and send it to the fragment shader
					v.vertex.xyz = vPos.xyz;
			}


			void surf(Input IN, inout SurfaceOutput o) {
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
				o.Albedo = c.rgb;
				o.Alpha = c.a;
			}
			ENDCG



		}
		FallBack "VertexLit"
}
