﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Daniel/Triplanar/Triplanar Displacement" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DisplaceTex("Displacement", 2D) = "black" {}
		_Tessellation("Tessellation", Range(1,32)) = 1
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0

		_DisplaceStrength("Displacement Strenth", range(0, 1)) = 0
        _TextureScale("Texture Scale", range(0,4)) = 1
        _BlendSharpness("Blend Sharpness", range(0,8)) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard addshadow vertex:vert tessellate:tess

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _DisplaceTex;

		float4 _DisplaceTex_ST;
		float _Tessellation;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		half _DisplaceStrength;

		fixed4 _Color;

        float _TextureScale;
        float _BlendSharpness;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		float4 tess()
		{
			return _Tessellation;
		}

		void vert(inout appdata_full v)
		{
			float3 worldPos = mul(unity_ObjectToWorld, v.vertex);
			float3 worldNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;

			_DisplaceTex_ST.w += _Time.y * 0.25;
			_DisplaceTex_ST.w += _Time.x;

			//Find the UV for each axis based on fragment world coordinates
			half2 yUV = (worldPos.xz / _TextureScale) + _DisplaceTex_ST.zw;

			half2 xUV = worldPos.zy / _TextureScale + _DisplaceTex_ST.zw;

			half2 zUV = worldPos.xy / _TextureScale + _DisplaceTex_ST.zw;


			//Sample textures with each plane of coordinates

			half3 ySample = tex2Dlod(_DisplaceTex, float4(yUV, 0, 0));
			half3 xSample = tex2Dlod(_DisplaceTex, float4(xUV, 0, 0));
			half3 zSample = tex2Dlod(_DisplaceTex, float4(zUV, 0, 0));

			// Get the absolute value of the world normal.
			// Put the blend weights to the power of BlendSharpness, the higher the value, the sharper the transition between the planar maps will be.

			half3 blendWeights = pow(abs(worldNormal), _BlendSharpness);

			//Normalize our blend weights
			blendWeights = normalize(blendWeights);

			//Blend together our 3 planar texture samples base on the mask
			fixed3 displacement = xSample * blendWeights.x + ySample * blendWeights.y + zSample * blendWeights.z;

			v.vertex.xyz += displacement.r * _DisplaceStrength * v.normal;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
        {
            

			//Set the usual outputs as per textures
			o.Albedo = tex2D(_MainTex, IN.uv_MainTex);
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = 1;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
