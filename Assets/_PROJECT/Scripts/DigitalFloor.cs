﻿using UnityEngine;

public class DigitalFloor : MonoBehaviour {

    public Transform m_Transform;
    public Renderer m_Renderer;

	void Update ()
    {
        m_Renderer.sharedMaterial.SetVector("_Target", m_Transform.position);
	}
}
