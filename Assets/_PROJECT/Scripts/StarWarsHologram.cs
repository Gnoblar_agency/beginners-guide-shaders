﻿using UnityEngine;

public class StarWarsHologram : MonoBehaviour
{
    public Renderer m_Renderer;

	// Update is called once per frame
	void Update ()
    {
        m_Renderer.sharedMaterial.SetVector("_Right", Camera.main.transform.right);
	}
}
